(defproject asktodon "0.1.0-SNAPSHOT"
  :description "Asktodon"
  :url nil
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.9.0"]      ; We need a language, right?
                 [org.clojure/core.async "0.4.490"] ; Async stuff for I/O
                 [ring "1.7.1"]                     ; HTTP server and interface
                 [ring/ring-defaults "0.3.2"]       ; Simple default middleware
                 [compojure "1.6.1"]                ; Simple routing
                 [hiccup "1.0.5"]                   ; HTML templating
                 [clj-http "3.9.1"]                 ; OAuth and HTTP-client
                                                    ;   (need to register app in Mastodon instances)
                 [cheshire "5.8.1"]                 ; JSON stuff
                 [mount "0.1.15"]                   ; Handle state? TODO not in use yet
                 [funcool/struct "1.3.0"]           ; Validation
                 ]
  :plugins [[lein-ring "0.12.5"]]
  :main asktodon.core
  :profiles {:uberjar {:uberjar-name "asktodon.jar"}}
  :repl-options {:port 7000}

  :ring {:handler asktodon.handler/app
         :auto-reload? true
         :auto-refresh? true
         :nrepl {:start? true
                 :port   7000
                 :host   "localhost"}})
