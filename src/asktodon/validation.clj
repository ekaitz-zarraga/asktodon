(ns asktodon.validation
    (:require [struct.core :as st]))

; TODO
; - All error messages need i18n and formatting to be able to inject them
; directy in the views

(def multiple-in
  {:message "Values not in options"
   :validate (fn [v ref]
               (if (coll? v)
                 (every? (set ref) v)
                 ((set ref) v)))})
(comment
  (st/validate {:hola 1}           {:hola [[multiple-in [1 2 3]]]})
  (st/validate {:hola [1 2 3 3 3]} {:hola [[multiple-in [1 2 3]]]})
  (st/validate {:hola [3 2 4]}     {:hola [[multiple-in [1 2 3]]]})
  (st/validate {:hola 4}           {:hola [[multiple-in [1 2 3]]]}))

(defn create-field-validator
  [field]
  (case (:type field)
    :title nil
    :separator nil
    :text (if (:required field)
            [(:name field) [[st/required] [st/string] [st/min-count 1]]]
            [(:name field) [[st/string]]])
    :text-area (if (:required field)
                 [(:name field) [[st/required] [st/string] [st/min-count 1]]]
                 [(:name field) [[st/string]]])
    :email (if (:required field)
             [(:name field) [[st/required] [st/email]]]
             [(:name field) [[st/email]]])
    :select-one (if (:required field)
                  [(:name field)
                   [[st/required]
                   [st/member (vec (map :name (:fields field)))]]]
                  [(:name field)
                   [[st/member (vec (map :name (:fields field)))]]])
    :select-multiple [(:name field) [[multiple-in (vec (map :name (:fields field)))]]]))

(defn create-validator
  [campaign]
  (into {} (map create-field-validator (:fields campaign))))

(defn- insert-data-in-campaign
  ; Check if there's any error like:
  ; (some :error (:fields full-campaign))
  [camp-desc [errors data]]
  (assoc camp-desc
         :fields (->> (:fields camp-desc)
                      (map #(if-let [error (get (or errors {}) (:name %1))]
                              (assoc %1 :error error)
                              %1))
                      (map #(if-let [data (get (or data {}) (:name %1))]
                              (assoc %1 :data data)
                              %1)))))

(defn validate-and-merge-campaign
  [campaign form-values]
  (->> (create-validator campaign)
       (st/validate form-values)
       (insert-data-in-campaign campaign)))


(comment
  (def campaign
    {:title "Job offer"
     :fields
     [
      {:type :title
       :description "Job Offer Survey"
       :name "main-title"
       }
      {:description "Name"
       :name "name"
       :type :text
       :required true}
      {:description "Full Description"
       :name "full-description"
       :type :text-area
       :required true
       :placeholder "Enter Full Description Please"
       }
      {:type :separator}
      {:type :select-one
       :name "favorite-animal"
       :description "Choose your favorite animal"
       :required true
       :fields [{:name "dog" :description "Dog is your favorite"}
                {:name "cat" :description "Cat is your favorite"}]
       }
      {:type :select-multiple
       :name "favorite-animal-2"
       :description "Choose your favorite animal"
       :fields [{:name "dog" :description "Dog is your favorite"}
                {:name "cat" :description "Cat is your favorite"}]
       }
      ]})
  (def camp-val (create-validator campaign))
  (def result
    {"_method" "POST"
     "__anti-forgery-token" "oQRBYPn/9TTVR/U4xx7vrGUUzsNL2XB0t+nnrq8eTJW0UjeLHA7ZTm+8Gp6Y17Aa61gX6YeLy23CxphT"
     "name" "adasda"
     "full-description" "asdasida"
     "favorite-animal" "horse" ; valid are only "dog" and "cat"
     "favorite-animal-2" ["dog" "cat"]})
  (st/validate result camp-val))

; RETURNS:
;[{"favorite-animal" "not in coll"}
; {"_method" "POST",
;  "__anti-forgery-token"
;  "oQRBYPn/9TTVR/U4xx7vrGUUzsNL2XB0t+nnrq8eTJW0UjeLHA7ZTm+8Gp6Y17Aa61gX6YeLy23CxphT",
;  "name" "adasda",
;  "full-description" "asdasida",
;  "favorite-animal-2" ["dog" "cat"]}]
