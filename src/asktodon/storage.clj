(ns asktodon.storage
  (:require [clojure.core.async :refer [<! put! chan go-loop]]
            [hiccup.util :refer [url-encode]]
            [clojure.java.io :refer [file]]
            [clojure.string :as s]))

; TODO: Move this to a configuration file
(defonce datadir "data")
(defonce client-data-file (file datadir "client-data.edn"))

;;;;;;;;;;;;;;;;

(defn- load-client-data
  "Load client data file, supposed to be done only once."
  [client-data-file]
  (try
    (read-string (slurp client-data-file))
    (catch Exception e
      (do
        (println "[WARNING] Unable to read client data: starting empty.")
        {}))))

(defn- listen-client-data
  "Intercept client-data atom and update it to file."
  [client-data-file]
  (let [channel (chan 10)]
    (go-loop []
             (when-some [data (<! channel)]
               (try
                 (spit client-data-file data)
                 (catch Throwable t
                   (println "Unable to write the file" t)))
               (recur)))
    channel))

(defn- clean-string
  "Converts string to lowercase and it removes any non alphabet character."
  [string]
  (-> string
      .toLowerCase
      (s/replace #"[^a-z^0-9]" "_")))

(defn- prep-filename
  "Prepares a root and any amount of filenames into a tree like path and treats
  the last filename as an edn file. Adds the .edn extension.
  If there are not filenames it returns nil."
  [root & fnames]
  (let [clean (some->> fnames
                       (map clean-string)
                       (apply file root))]
    (when clean (file (str clean ".edn")))))

(defn- mkdirs!
  "Creates a directory tree by name in the selected location.
  Returns the written directory as a java.file or nil if nothing was
  written."
  [filename]
  (try
      (and (or (.mkdirs (file filename)) nil) filename)
    (catch Exception e
      false)))

;;;;;;;;;;;;;;;;

(defonce ^{:doc
  "Data atom that it's automatically persisted to file when it's
 changed. It's loaded with the content from the file automatically, if
 the file doesn't exist at the moment this atom is created it's
 initialized as an empty map."}
  client-data
  (add-watch
    (atom (load-client-data client-data-file))
    identity
    (let [chan (listen-client-data client-data-file)]
      #(put! chan %4))))

(comment
  "Reference format for client data storage"
  {:instances {
    "plaza.remolino.town" {
        :client_id "asdf"
        :client_secret "asdf"
        ; ...
    }
  }})

(defn write-answer!
  "Write the form answer to file.
  File is written in campaign/user_date.edn file"
  [campaign user answer]
  (let [filename (prep-filename datadir
                                campaign
                                (str user " " (System/currentTimeMillis)))
        dir      (.getParentFile filename)]
    (try (when-not (.exists dir)
           (mkdirs! dir))
         (spit filename answer)
         (catch Exception e
           :error))))

(defn load-campaign!
  [campaign]
  (try
    (->> (str (clean-string campaign) ".edn")
         (file datadir "campaigns")
         slurp
         read-string)
    (catch Exception e
      nil)))


;;;;;;;;;;;;;;;;

(def get-or-reload-campaign!
  (let [campaigns (atom {})]
    (fn [campaign]
      (let [campaign-file (->> (str (clean-string campaign) ".edn")
                               (file datadir "campaigns"))
            campaign-cached (@campaigns campaign)
            last-modif      (.lastModified campaign-file)]
        (if (and (some? campaign-cached)
                 (<= last-modif (:last-modified campaign-cached)))
          campaign-cached
          ((swap! campaigns
                  assoc  campaign (-> campaign
                                      load-campaign!
                                      (assoc :last-modified last-modif)))
           campaign))))))
