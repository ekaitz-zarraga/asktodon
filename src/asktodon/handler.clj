(ns asktodon.handler
  (:require [compojure.core :refer [GET POST defroutes routes]]
            [compojure.route :as route]
            [ring.util.response
             :refer [response content-type redirect status not-found]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [asktodon.views :as vws]
            [asktodon.storage
             :refer [client-data write-answer! load-campaign!]]
            [asktodon.validation :refer [validate-and-merge-campaign]]
            [asktodon.client :refer [register-app! authorize-app-link get-access-token! get-user-info!]]
            [clojure.string :refer [split]]
            [clojure.pprint :refer [pprint]];TODO
            ))

(defn- ok-html
  [content]
  (-> content
      response
      (content-type "text/html")))

(defn- not-found-html
  [content]
  (-> content
      not-found
      (content-type "text/html")))

(defn- extract-domain-from-handle
  [handle]
  (last (split handle #"@")))

(comment
  (extract-domain-from-handle "@sirikon@plaza.remolino.town"))

(defn- get-instance-client-from-data
  [data instance]
  (some-> data
          :instances
          (get instance)))

(defn- ensure-mastodon-client!
  [instance]
  (locking client-data
    (if-let [client (get-instance-client-from-data @client-data instance)]
      client
      (get-instance-client-from-data
        (swap! client-data assoc-in [:instances instance]
          (register-app! (str "https://" instance)))
        instance))))

(defroutes app-routes
  (GET "/" []
       (fn [req]
           (ok-html (str "Hello World " (get (:session req) :username)))))


  (GET "/identify"
       []
       (fn [req]
         (ok-html (vws/identification-form (:anti-forgery-token req)))))

  (POST "/submit-identification"
        [:as {qparams :query-params fparams :form-params}]
        (fn [req]
          (let [domain (extract-domain-from-handle (fparams "handle"))
                client (ensure-mastodon-client! domain)]
            (-> (authorize-app-link client (str "https://" domain) domain)
                redirect))))

  (GET "/submit-identification-callback"
       [:as {qparams :query-params}]
       (fn [req]
        (let [code   (get qparams "code")
              domain (get qparams "state")
              url    (str "https://" domain)]
          (-> (redirect "/" :see-other)
              (assoc :session (:session req))
              (assoc-in [:session :username]
                        (str "@"
                             (->> (get-access-token!
                                    url
                                    (ensure-mastodon-client! domain)
                                    code)
                                  (get-user-info! url)
                                  :username)
                             "@"
                             domain))))))

  (GET "/campaigns/:campaign"
       [campaign]
       (fn [req]
         (if-let [username (:username (:session req))]
           (if-let [camp-fields (load-campaign! campaign)]
             (ok-html
               (vws/campaign-form (:anti-forgery-token req)
                                  campaign
                                  camp-fields))
             (not-found-html
               (vws/campaign-not-found {:campaign campaign})))

           (-> (redirect "/identify")
               (assoc     :session (:session req))
               (assoc-in  [:session :redirect-to-campaign] campaign)))))

  (POST "/campaigns/:campaign"
        [campaign :as {qparams :query-params fparams :form-params}]
        (println fparams)
        (fn [req]
          (if-let [username (:username (:session req))]
            (if-let [camp-desc (load-campaign! campaign)]
              (let [full-camp (validate-and-merge-campaign camp-desc fparams)]
                (if (some :error (:fields full-camp))
                  (ok-html
                    (vws/campaign-form (:anti-forgery-token req)
                                       campaign
                                       full-camp))

                  (case (write-answer! campaign
                                       username
                                       (merge {:username username} fparams))
                    nil    (vws/campaign-stored-successfully)
                    :error (-> (response nil)
                               (status 500)))))

              (not-found-html
                (vws/campaign-not-found {:campaign campaign})))

              (-> (response nil)
                  (status 401))))))

(defonce app
  (routes
    (route/resources "/")
    ; There's a problem in the redirection chain between instance and Asktodon,
    ; in which the cookies associated with the requests aren't being sent after
    ; a couple of redirects. Seems like configuring the cookie's samesite
    ; setting to 'Lax' fixes the issue atm.
    ; https://stackoverflow.com/questions/19047141/cookies-not-sent-after-a-302-redirect-to-a-page-on-the-same-domain
    ; https://developer.mozilla.org/es/docs/Web/HTTP/Headers/Set-Cookie
    (wrap-defaults app-routes (assoc-in site-defaults [:session :cookie-attrs :same-site] :lax ))
    (route/not-found "Not Found")))
