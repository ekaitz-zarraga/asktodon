(ns asktodon.views
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.form :refer :all]))

(defn application
  [title & content]
  (html5 {:ng-app "AskTodon" :lang "en"}
         [:head
          [:title (str "Asktodon - " title)]
          (include-css "/css/spectre.css")
          (include-css "/css/style.css")
          ;(include-js "/js/script.js") ; NOPE!
          [:body [:div {:class "container"} content ]]]))

(defn- class-str
  "Creates a class string from a collection. Needed for Hiccup."
  [classes]
  (apply str (interpose " " classes)))

(defn identification-form
  [anti-forgery-token]
  (application
    "Identification"
    (form-to {:class "simple-form"}
             [:POST "/submit-identification"]
             (hidden-field "__anti-forgery-token" (force anti-forgery-token))
             (label {:class "form-label" :for "handle"} "handle" "Insert your full handle")
             (text-field {:placeholder "example@mastodon.instance"} "handle")
             (submit-button {:class "btn btn-primary"} "Submit"))))

(defn- form-field
  [form-el-type field]
  [:div {:class "form-group"}
   (label {:class "form-label" :for (:name field)}
          (:name field)
          (:description field))

   (form-el-type {:class       (->> ["form-input" "full-with"]
                                    (#(if (:error field)
                                      (conj %1 "is-error") %1))
                                    class-str)
                  :placeholder (:placeholder field)
                  :required    (:required field)
                  :value       (:data field)}
                 (:name field))
   (when-let [error (:error field)]
     [:p.form-input-hint error])])

(defn- text-area-field
  [field]
  [:div {:class "form-group"}

   (label {:class "form-label" :for (:name field)}
          (:name field)
          (:description field))

   (text-area {:class       (->> ["form-input" "full-with"]
                                    (#(if (:error field)
                                      (conj %1 "is-error") %1))
                                    class-str)
               :placeholder (:placeholder field)
               :required    (:required field)}
              (:name field)
              (when-let [data (:data field)] data))
   (when-let [error (:error field)]
     [:p.form-input-hint error])])


(defn labeled-radio
  [group-name required checked field-info]
  [:label {:class "form-radio"}
         (radio-button {:required required} group-name checked (:name field-info))
         [:i {:class "form-icon"}]
         (:description field-info)])

(defn labeled-check-box
  [group-name checked field-info]
  [:label {:class "form-checkbox"}
         (check-box group-name checked (:name field-info))
         [:i {:class "form-icon"}]
         (:description field-info)])

(defn select-one
  [field]
  [:div {:class   (->> ["form-group"]
                       (#(if (:error field)
                           (conj %1 "has-error") %1))
                       class-str)}
   (label {:class "form-label" :for (:name field)}
          (:name field)
          (:description field))
   (map #(labeled-radio (:name field)
                        (:required field)
                        (= (:data field) (:name %1))
                        %1)
        (:fields field))
   (when-let [error (:error field)]
     [:p.form-input-hint error])])

(defn select-multiple
  [field]
  [:div {:class   (->> ["form-group"]
                       (#(if (:error field)
                           (conj %1 "has-error") %1))
                       class-str)}
   (label {:class "form-label" :for (:name field)}
          (:name field)
          (:description field))
   (map
     (fn [f]
       (labeled-check-box (:name field)
                          (some #(= (:name f)) (:data field))
                          f))
     (:fields field))

   (when-let [error (:error field)]
     [:p.form-input-hint error])])

(defn campaign-field
  [field]
   (case (:type field)
     :text
     (form-field text-field field)

     :email
     (form-field email-field field)

     :text-area
     (text-area-field field)

     :select-one
     (select-one field)

     :select-multiple
     (select-multiple field)

     :separator
     [:hr]

     :title
     [:h3 {:id (:name field)} (:description field)]

     (println "Unrecognized campaign field type: " (:type field))))

(defn campaign-form
  [anti-forgery-token campaign-name campaign-data]
  ; TODO Fill it with the data and the errors if they are present!
  ; Spectre-css needs:
  ; - is-error class in the label or the input
  ; - and [:p {class "form-input-hint"} "Error message."] inside the form-group
  ;   for the error message
  (application
    (:title campaign-data)
    [:div {:class "full-width"}
     (form-to [:POST (str "/campaigns/" campaign-name)]
              (hidden-field "__anti-forgery-token" (force anti-forgery-token))
              (map campaign-field (:fields campaign-data))
              (submit-button {:class "btn btn-primary"} "Submit"))])) ;TODO i18n



(defn campaign-stored-successfully ; TODO i18n
  [& data]
  (application "Campaign stored successfully"
               [:h1 "Campaign stored successfully"]))

(defn campaign-not-found ; TODO i18n
  [& data]
  (application "Not Found" [:h1 "404: Page not found"]))
