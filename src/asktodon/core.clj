(ns asktodon.core
  (:require [asktodon.handler :refer [app]]
            [ring.adapter.jetty :as jetty]
            [mount.core :refer [defstate]]
            ;[asktodon.storage :refer [client-data]]
            )
  (:gen-class))

; TODO MOUNT THIS SHIT!
(comment (defstate http-server
           :start start-function
           :stop (.stop http-server)))
(comment (defstate database
           :start ()
           :stop  ()))



(declare http-server)

(defn -main
  "A very simple web server using Ring & Jetty"
  [& args]
  ; TODO Check this thingie
  ; https://stackoverflow.com/questions/2706044/how-do-i-stop-jetty-server-in-clojure
  ; And this one too
  ; https://crossclj.info/ns/luminus-jetty/latest/luminus.http-server.html

  (defonce http-server
           (jetty/run-jetty
             app
             ; TODO: change this!
             {:port 3000
              :host "127.0.0.1"
              :join? false})))
