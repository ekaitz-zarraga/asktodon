(ns asktodon.client
  (:require [clj-http.client :as client]
            [hiccup.util :refer [url to-str]]
            [asktodon.storage :refer [client-data]]))

(def client-name   "AskTodon")
(def callback-uri  "http://localhost:3000/submit-identification-callback")
(def redirect-uris (str "urn:ietf:wg:oauth:2.0:oob\n" callback-uri ))
(def scopes        "read:accounts")
(def website       nil)

(defn register-app!
  [instance]
  (:body (client/post (str instance "/api/v1/apps")
                      {:as :json
                       :with-credentials false
                       :form-params      {:client_name   client-name
                                          :redirect_uris redirect-uris
                                          :scopes        scopes
                                          :website       website}})))


(defn authorize-app-link
  [client-params instance csrf-token]
  (to-str
    (url 
      instance "/oauth/authorize"
      {:response_type "code"
       :client_id (:client_id client-params)
       :redirect_uri callback-uri
       :scope scopes
       :state csrf-token})))


(defn get-access-token!
  "Gets an access-token from the autorization-code. In mastodon access tokens
  never expire. There's no need to refresh them."
  [instance client code]
  (get (:body (client/post (to-str (url instance "/oauth/token"))
                      {:form-params {:code         code
                                     :grant_type   "authorization_code"
                                     :client_id    (:client_id client)
                                     :redirect_uri callback-uri}
                       :basic-auth  [(:client_id client) (:client_secret client)]
                       :as          :json})) :access_token))



(defn get-user-info!
  "User info API call"
  [instance access-token]
  (:body (client/get (to-str (url instance "/api/v1/accounts/verify_credentials"))
              {:oauth-token access-token :as :json})))



(comment

  (let [client (register-app! "https://plaza.remolino.town")]
    (println (authorize-app-link "https://plaza.remolino.town" "csrfshit")))

  (-> 
    (register-app! "https://plaza.remolino.town")
    (authorize-app-link "https://plaza.remolino.town" "csrfshit"))

  (register-app! "https://plaza.remolino.town")
  (def answer {:id "433773"
               :name "AskTodon"
               :website nil
               :redirect_uri "urn:ietf:wg:oauth:2.0:oob"
               :client_id "662d51cfe753c7b57d0c9cd9320c8a4036a48600012dc14689d118cdd20105fc"
               :client_secret "0ff3af172a504dca67ea9014ff092a1e781991f9f5535b6724bce4445ec98ed1"}))
(comment (authorize-app-link answer "https://mastodon.social" "csrfshit"))

(comment get-user-info!)
